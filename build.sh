#!/bin/bash

TARGET=${1%/}
BASEDIR="$(cd "$(dirname "${0}")" && pwd)"
SITEDIR="${BASEDIR}/${TARGET}"

if [ ! -d "${SITEDIR}" ]; then
  echo "Usage: ${0##*/} < sandro.mathys.io[/] | blog.mathys.io[/] | photography.mathys.io[/] >"
  exit 1
fi

echo "Installing gems"
bundle install --path vendor/bundle || exit 1

echo "Building ${TARGET}"

pushd "${SITEDIR}" &> /dev/null
  # use unbuffer to preserve colors when piping to tee
  unbuffer bundle exec middleman build --verbose |& tee build.log
popd &> /dev/null
