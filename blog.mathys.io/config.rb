Time.zone = 'UTC'

activate :directory_indexes
set :trailing_slash, true

set :css_dir, 'css'
set :js_dir, 'js'
set :images_dir, 'img'

page '/js/*', :layout => false
page 'posts/*', :layout => :post
page '*', :layout => :blog

ignore '/icons/*.png'
ignore '/icons/*.svg'
ignore '/icons/*.ico'
[ 'favicon.ico', 'favicon-16x16.png', 'favicon-32x32.png', 'favicon-96x96.png',
  'android-chrome-36x36.png', 'android-chrome-48x48.png',
  'android-chrome-72x72.png', 'android-chrome-96x96.png',
  'android-chrome-144x144.png', 'android-chrome-192x192.png',
  'apple-touch-icon-precomposed.png', 'apple-touch-icon.png',
  'apple-touch-icon-57x57.png', 'apple-touch-icon-60x60.png',
  'apple-touch-icon-72x72.png', 'apple-touch-icon-76x76.png',
  'apple-touch-icon-114x114.png', 'apple-touch-icon-120x120.png',
  'apple-touch-icon-144x144.png', 'apple-touch-icon-152x152.png',
  'apple-touch-icon-180x180.png', 'safari-pinned-tab.svg',
  'mstile-70x70.png', 'mstile-144x144.png', 'mstile-150x150.png',
  'mstile-310x150.png', 'mstile-310x310.png' ].each do |filename|
     proxy "#{filename}", "/icons/#{filename}"
end

activate :blog do |blog|
  blog.sources = "posts/{year}-{month}-{day}-{title}.html"
  blog.permalink = '{year}/{month}/{day}/{title}.html'
  blog.tag_template = 'tag.html'
  blog.calendar_template = 'calendar.html'
  blog.paginate = true
  blog.per_page = 5
end

def get_tags(resource)
  if resource.data.tags.is_a? String
    resource.data.tags.split(',').map(&:strip)
  else
    resource.data.tags
  end
end

def group_lookup(resource, sum)
  results = Array(get_tags(resource)).map(&:to_s)

  results.each do |k|
    sum[k] ||= []
    sum[k] << resource
  end
end

tags = resources
  .select { |resource| resource.data.tags }
  .each_with_object({}, &method(:group_lookup))

tags.each do |tag, articles|
  proxy "/tags/#{tag.downcase}/atom.xml", "/atom.xml", locals: { tagname: tag, articles: articles[0..10] }
end

configure :build do
  activate :minify_css
  activate :minify_javascript
  activate :relative_assets
end
