activate :directory_indexes
set :trailing_slash, true

set :css_dir, 'css'
set :js_dir, 'js'
set :images_dir, 'img'

page '/js/*', :layout => false
page '/*', :layout => :resume

ignore '/icons/*.png'
ignore '/icons/*.svg'
ignore '/icons/*.ico'
[ 'favicon.ico', 'favicon-16x16.png', 'favicon-32x32.png', 'favicon-96x96.png',
  'android-chrome-36x36.png', 'android-chrome-48x48.png',
  'android-chrome-72x72.png', 'android-chrome-96x96.png',
  'android-chrome-144x144.png', 'android-chrome-192x192.png',
  'apple-touch-icon-precomposed.png', 'apple-touch-icon.png',
  'apple-touch-icon-57x57.png', 'apple-touch-icon-60x60.png',
  'apple-touch-icon-72x72.png', 'apple-touch-icon-76x76.png',
  'apple-touch-icon-114x114.png', 'apple-touch-icon-120x120.png',
  'apple-touch-icon-144x144.png', 'apple-touch-icon-152x152.png',
  'apple-touch-icon-180x180.png', 'safari-pinned-tab.svg',
  'mstile-70x70.png', 'mstile-144x144.png', 'mstile-150x150.png',
  'mstile-310x150.png', 'mstile-310x310.png' ].each do |filename|
     proxy "#{filename}", "/icons/#{filename}"
end

configure :build do
  activate :minify_css
  activate :minify_javascript
  activate :relative_assets
end
